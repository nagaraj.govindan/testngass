package com.test;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class DemoLogin {
	WebDriver driver;
	
		 @Given("open the website")
		 public void open_the_website() {
			 driver=new ChromeDriver();
			 driver.manage().window().maximize();
			 driver.get("https://demowebshop.tricentis.com/login");
			 System.out.println("lauching the browser");
		 }
		 @Given("The homepage of the website get displayed")
		 public void the_homepage_of_the_website_get_displayed() {
		  
				System.out.println("Home is displayed");
		 }
		 @When("Click on the login profile button")
		 public void click_on_the_login_profile_button() {
				System.out.println("click login profile button");
			 
		 }
		 @When("Use should enter {string} the valid Username")
		 public void use_should_enter_the_valid_username(String string) {
		    
				driver.findElement(By.id("Email")).sendKeys(string);
		 }
		 @When("User enter {string} the valid Password")
		 public void user_enter_the_valid_password(String string) {
			 driver.findElement(By.id("Password")).sendKeys(string);
			 
		 }
		 @Then("Click on the login button")
		 public void click_on_the_login_button() {
			 driver.findElement(By.xpath("//input[@value='Log in']")).click();
				System.out.println("click on login");
			 
		 }
		 @Then("The user should taken to the homepage of the demo web shop")
		 public void the_user_should_taken_to_the_homepage_of_the_demo_web_shop() {
			 System.out.println("homepage of demo wed shop sholud be displayed");
			 
		 }
		 @Then("close the browser")
		 public void close_the_browser() {
		  
				driver.close();
				System.out.println("close the website");
		 }
		
	}
		
	